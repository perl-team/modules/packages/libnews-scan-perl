libnews-scan-perl (0.53-5) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 08 Jul 2019 18:17:05 +0000

libnews-scan-perl (0.53-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 13:21:16 +0100

libnews-scan-perl (0.53-4) unstable; urgency=medium

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Chris Butler from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Do not ship useless README
  * Update license paragraphs to commonly used versions
  * Add myself to Uploaders
  * Bump dh compat to level 11
  * Brush up package description
  * Add patch header
  * Fix interpreter path in example scripts
  * Declare compliance with Debian Policy 4.1.5

 -- Florian Schlichting <fsfs@debian.org>  Sat, 07 Jul 2018 20:32:24 +0200

libnews-scan-perl (0.53-3) unstable; urgency=low

  * Update standards version to 3.8.4
    - Added Homepage field to debian/control
  * Refreshed packaging
    - Switch to dh7
    - Updated debian/copyright to DEP5
  * Moving my package into the Debian Perl Group
    - added Vcs-Svn and Vcs-Browser fields.
    - changed Maintainer to the Debian Perl Group.
    - moved myself to Uploaders.
  * Switched to source format 3.0 (quilt)
  * Patched News/Scan.pm to fix pod errors
  * Added debian/watch file

 -- Chris Butler <chrisb@debian.org>  Mon, 29 Mar 2010 23:55:55 +0100

libnews-scan-perl (0.53-2) unstable; urgency=low

  * Fixed debian/rules to comply with perl policy
  * Upgraded to standards version 3.5.9.0

 -- Chris Butler <chrisb@debian.org>  Sun,  6 Apr 2003 19:04:40 +0100

libnews-scan-perl (0.53-1) unstable; urgency=low

  * New upstream release (fixes: #180540)
  * Uses dh_perl in debian/rules (fixes: #152226)
  * Changed section from 'news' to 'perl'

 -- Chris Butler <chrisb@debian.org>  Fri,  4 Apr 2003 17:39:00 +0100

libnews-scan-perl (0.51-2) unstable; urgency=low

  * Updated dependencies for mailtools (closes: #113023)

 -- Chris Butler <chrisb@crustynet.org.uk>  Sun, 11 Nov 2001 23:52:13 +0000

libnews-scan-perl (0.51-1) unstable; urgency=low

  * Initial Release.

 -- Chris Butler <chrisb@debian.org>  Thu,  7 Dec 2000 17:33:17 +0000

Local variables:
mode: debian-changelog
add-log-mailing-address "chrisb@debian.org"
End:
